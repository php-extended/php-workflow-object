# php-extended/php-workflow-object

A library that implements the php-extended/php-workflow-interface package

![coverage](https://gitlab.com/php-extended/php-workflow-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-workflow-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-workflow-object ^8`


## Basic Usage

Lets say you want to express the workflow of a door (which can be opened or closed).
You have the following schema :

	+--------+    open    +--------+
	|        | ---------> |        |
	| closed |            | opened |
	|        | <--------- |        |
	+--------+   close    +--------+

Then, you'll have to have the following classes:

use PhpExtended\Workflow\SubjectInterface;
use PhpExtended\Workflow\StateInterface;
use PhpExtended\Workflow\ConditionInterface;
use PhpExtended\Workflow\WorkflowBuilder;

class Door implements SubjectInterface
{
	
	public $_state = null;
	
	public function __construct()
	{
		$this->_state = new Closed();
	}
	
	public function getState(WorkflowInterface $workflow) : StateInterface
	{
		if($workflow->getName() === 'usage')
		{
			return $this->_state;
		}
		return new Closed();
	}
	
	public function execute(TransitionInterface $transition) : StateInterface
	{
		if($transition->getName() === 'open')
		{
			return $this->_state = new Opened();
		}
		
		if($transition->getName() === 'close')
		{
			return $this->_state = new Closed();
		}
		
		return new Closed();
	}
	
}

class Opened implements StateInterface;
{
	
	public function getName() : string
	{
		return 'opened';
	}
	
}

class Closed implements StateInterface
{
	
	public function getName() : string
	{
		return 'closed';
	}
	
}

class DoorIsClosed implements ConditionInterface
{
	
	public function isSatisfiedBy(SubjectInterface $subject) : bool
	{
		return $subject instanceof Door
		    && $subject->getState() instanceof Closed;
	}
	
}

class DoorIsOpened implements ICondition
{
	
	public function isSatisfiedBy(ISubject $subject) : bool
	{
		return $subject instanceof Door
		       $subject->getState() instanceof Opened;
	}
	
}

$definition = new Definition('usage');
$opened = new Opened();
$closed = new Closed();
$open   = new Transition('open', $opened, $closed);
$close  = new Transition('close', $closed, $opened);
$workflowBuilder->addCondition('open', new DoorIsClosed());
$workflowBuilder->addTransition('close', $opened, $closed);
$workflowBuilder->addCondition('close', new DoorIsOpened());

$workflow = $workflowBuilder->getWorkflow('usage');

$door = new Door();	// at instanciation, door is closed
$workflow->can($door, $closed); 	// returns false
$workflow->car($door, $opened);		// returns true
$workflow->perform($door, $opened);	// door is now opened
$workflow->can($door, $opened); 	// returns false
$workflow->can($door, $closed);		// returns true
$workflow->perform($door, $closed); // door is now closed


## License

MIT (See [license file](LICENSE)).
