<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

/**
 * ConditionOr class file.
 *
 * This condition is passing if at least one of its inner condition is passing,
 * or if there is no inner conditions.
 *
 * @author Anastaszor
 */
class ConditionOr implements ConditionInterface
{
	
	/**
	 * All the inner conditions for this one.
	 *
	 * @var array<integer, ConditionInterface>
	 */
	protected array $_conditions = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a new condition to this condition array.
	 *
	 * @param ConditionInterface $condition
	 */
	public function addCondition(ConditionInterface $condition) : void
	{
		$this->_conditions[] = $condition;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\ConditionInterface::isSatisfiedBy()
	 */
	public function isSatisfiedBy(SubjectInterface $subject) : bool
	{
		if(0 === \count($this->_conditions))
		{
			return true;
		}
		
		foreach($this->_conditions as $condition)
		{
			if($condition->isSatisfiedBy($subject))
			{
				return true;
			}
		}
		
		return false;
	}
	
}
