<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use ArrayIterator;
use Iterator;

/**
 * Definition class file.
 *
 * This class defines a simple Definition.
 *
 * @author Anastaszor
 */
class Definition implements DefinitionInterface
{
	
	/**
	 * The name of the workflow.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * All the states in the defined workflow.
	 *
	 * @var array<string, StateInterface>
	 */
	protected array $_states = [];
	
	/**
	 * All the transitions in the defined workflow.
	 *
	 * @var array<string, TransitionInterface>
	 */
	protected array $_transitions = [];
	
	/**
	 * Builds a new Definition with the given name.
	 *
	 * @param string $name
	 * @param array<integer, StateInterface> $states
	 * @param array<integer, TransitionInterface> $transitions
	 */
	public function __construct(string $name, array $states = [], array $transitions = [])
	{
		$this->_name = $name;
		
		foreach($states as $state)
		{
			$this->addState($state);
		}
		
		foreach($transitions as $transition)
		{
			$this->addTransition($transition);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a state to the known states of this workflow.
	 *
	 * @param StateInterface $state
	 */
	public function addState(StateInterface $state) : void
	{
		$this->_states[$state->getName()] = $state;
	}
	
	/**
	 * Adds a transition to the known states of this workflow.
	 *
	 * @param TransitionInterface $transition
	 */
	public function addTransition(TransitionInterface $transition) : void
	{
		$this->_transitions[$transition->getName()] = $transition;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\DefinitionInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\DefinitionInterface::getStates()
	 */
	public function getStates() : Iterator
	{
		return new ArrayIterator($this->_states);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\DefinitionInterface::getTransitions()
	 */
	public function getTransitions() : Iterator
	{
		return new ArrayIterator($this->_transitions);
	}
	
}
