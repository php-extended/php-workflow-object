<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

/**
 * ConditionAnd class file.
 *
 * This condition is passing if all of its inner conditions is passing.
 *
 * @author Anastaszor
 */
class ConditionAnd implements ConditionInterface
{
	
	/**
	 * All the inner conditions for this one.
	 *
	 * @var array<integer, ConditionInterface>
	 */
	protected array $_conditions = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a new condition to this condition array.
	 *
	 * @param ConditionInterface $condition
	 */
	public function addCondition(ConditionInterface $condition) : void
	{
		$this->_conditions[] = $condition;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\ConditionInterface::isSatisfiedBy()
	 */
	public function isSatisfiedBy(SubjectInterface $subject) : bool
	{
		foreach($this->_conditions as $condition)
		{
			if(!$condition->isSatisfiedBy($subject))
			{
				return false;
			}
		}
		
		return true;
	}
	
}
