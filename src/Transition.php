<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

/**
 * Transition class file.
 *
 * This class is a simple transition that supports conditions to be passing.
 *
 * @author Anastaszor
 */
class Transition implements TransitionInterface
{
	
	/**
	 * Gets the name of this transition.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Gets the state of the subject before the transition is done.
	 *
	 * @var StateInterface
	 */
	protected StateInterface $_fromState;
	
	/**
	 * Gets the state of the subject once the transition is done.
	 *
	 * @var StateInterface
	 */
	protected StateInterface $_destState;
	
	/**
	 * Gets the root condition. This should be an AND condition.
	 *
	 * @var ConditionAnd
	 */
	protected ConditionAnd $_rootCondition;
	
	/**
	 * Builds a new Transition object with the given from and to state.
	 *
	 * @param string $name
	 * @param StateInterface $from
	 * @param StateInterface $dest
	 */
	public function __construct(string $name, StateInterface $from, StateInterface $dest)
	{
		$this->_name = $name;
		$this->_fromState = $from;
		$this->_destState = $dest;
		$this->_rootCondition = new ConditionAnd();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a new condition to the this transition.
	 *
	 * @param ConditionInterface $condition
	 */
	public function addCondition(ConditionInterface $condition) : void
	{
		$this->_rootCondition->addCondition($condition);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\TransitionInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\TransitionInterface::getStartState()
	 */
	public function getStartState() : StateInterface
	{
		return $this->_fromState;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\TransitionInterface::getEndState()
	 */
	public function getEndState() : StateInterface
	{
		return $this->_destState;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\TransitionInterface::isSatisfiedBy()
	 */
	public function isSatisfiedBy(SubjectInterface $subject) : bool
	{
		return $this->_rootCondition->isSatisfiedBy($subject);
	}
	
}
