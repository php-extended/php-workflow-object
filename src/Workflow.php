<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use RuntimeException;

/**
 * Workflow class file.
 *
 * This class defines a simple workflow.
 *
 * @author Anastaszor
 */
class Workflow implements WorkflowInterface
{
	
	/**
	 * The definition of the workflow.
	 *
	 * @var DefinitionInterface
	 */
	protected DefinitionInterface $_definition;
	
	/**
	 * Gets the current state of the subjects, sorted by id.
	 * 
	 * @var array<string, StateInterface>
	 */
	protected array $_states = [];
	
	/**
	 * Builds a new workflow object.
	 *
	 * @param DefinitionInterface $definition
	 */
	public function __construct(DefinitionInterface $definition)
	{
		$this->_definition = $definition;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Registers a given subject to the given state.
	 * 
	 * @param SubjectInterface $subject
	 * @param StateInterface $initialState
	 * @return boolean true if registered, false if it was already
	 */
	public function registerSubject(SubjectInterface $subject, StateInterface $initialState) : bool
	{
		if(isset($this->_states[$subject->getId()]))
		{
			return false;
		}
		
		$this->_states[$subject->getId()] = $initialState;
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\WorkflowInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_definition->getName();
	}
	
	/**
	 * Gets the current state of the subject.
	 *
	 * @param SubjectInterface $subject
	 * @return StateInterface the current state of the subject
	 */
	public function getState(SubjectInterface $subject) : StateInterface
	{
		if(isset($this->_states[$subject->getId()]))
		{
			return $this->_states[$subject->getId()];
		}
		
		return new UnknownState();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\WorkflowInterface::can()
	 * @throws RuntimeException if the state of the subject cannot be found
	 */
	public function can(SubjectInterface $subject, TransitionInterface $transition) : bool
	{
		if(!isset($this->_states[$subject->getId()]))
		{
			throw new RuntimeException('Impossible to get the state from subject.');
		}
		
		$subjectState = $this->_states[$subject->getId()];
		
		return $subjectState->getName() === $transition->getStartState()->getName()
			&& $transition->isSatisfiedBy($subject);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\WorkflowInterface::perform()
	 * @throws RuntimeException if the expected state cannot be found on
	 *                          the subject
	 */
	public function perform(SubjectInterface $subject, TransitionInterface $transition) : StateInterface
	{
		if(!isset($this->_states[$subject->getId()]))
		{
			throw new RuntimeException('Impossible to get the state from subject.');
		}
		
		$subjectState = $this->_states[$subject->getId()];
		
		if($subjectState->getName() !== $transition->getStartState()->getName())
		{
			throw new RuntimeException(\strtr('The subject is not in the expected state "{name}".', ['{name}' => $transition->getStartState()->getName()]));
		}
		
		if(!$transition->isSatisfiedBy($subject))
		{
			throw new RuntimeException(\strtr('The subject does not satisfies the passing conditions of transition "{name}".', ['{name}' => $transition->getName()]));
		}
		
		return $this->_states[$subject->getId()] = $transition->getEndState();
	}
	
}
