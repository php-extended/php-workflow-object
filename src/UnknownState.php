<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

/**
 * UnknownState class file.
 * 
 * This class represents a default state for any object that is not registered
 * to a given workflow.
 * 
 * @author Anastaszor
 */
class UnknownState implements StateInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\StateInterface::getName()
	 */
	public function getName() : string
	{
		return 'UNKNOWN';
	}
	
}
