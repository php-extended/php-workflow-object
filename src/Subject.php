<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

/**
 * Subject class file.
 * 
 * This class represents an object that can change state regarding a specific
 * workflow.
 * 
 * @author Anastaszor
 */
class Subject implements SubjectInterface
{
	
	/**
	 * The id of the client.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * Builds a new Client.
	 * 
	 * @param string $ident
	 */
	public function __construct(string $ident)
	{
		$this->_id = $ident;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\SubjectInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\SubjectInterface::getState()
	 */
	public function getState(WorkflowInterface $workflow) : StateInterface
	{
		return $workflow->getState($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\SubjectInterface::can()
	 */
	public function can(WorkflowInterface $workflow, TransitionInterface $transition) : bool
	{
		return $workflow->can($this, $transition);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Workflow\SubjectInterface::perform()
	 */
	public function perform(WorkflowInterface $workflow, TransitionInterface $transition) : StateInterface
	{
		return $workflow->perform($this, $transition);
	}
	
}
