<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Workflow\Definition;
use PhpExtended\Workflow\Workflow;
use PHPUnit\Framework\TestCase;

/**
 * WorkflowTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Workflow\Workflow
 *
 * @internal
 *
 * @small
 */
class WorkflowTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Workflow
	 */
	protected Workflow $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Workflow(new Definition('name'));
	}
	
}
