<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Workflow\UnknownState;
use PHPUnit\Framework\TestCase;

/**
 * UnknownStateTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Workflow\UnknownState
 *
 * @internal
 *
 * @small
 */
class UnknownStateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UnknownState
	 */
	protected UnknownState $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('UNKNOWN', $this->_object->getName());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UnknownState();
	}
	
}
